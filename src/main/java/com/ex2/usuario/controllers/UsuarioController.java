package com.ex2.usuario.controllers;

import com.ex2.usuario.models.Usuario;
import com.ex2.usuario.services.UsuarioSerice;
import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

        @Autowired
        private UsuarioSerice usuarioService;

        @PostMapping
        @ResponseStatus(HttpStatus.CREATED)
        Usuario cadastrarUsuario(@RequestBody Usuario pusuario) {
            return usuarioService.cadastraUsuario(pusuario);
        }


        @GetMapping("/{id}")
        Usuario consultarPorId(@PathVariable(name = "id") int idUsuario) {

        try {
           return usuarioService.consultarUsuarioPorId(idUsuario);
         }
            catch (RuntimeException ex)
         {
            throw new RuntimeException(ex.getMessage());
         }
        }
}

