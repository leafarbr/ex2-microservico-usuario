package com.ex2.usuario.services;

import com.ex2.usuario.exceptions.UsuarioNaoEncontradoException;
import com.ex2.usuario.models.Usuario;
import com.ex2.usuario.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioSerice {


    @Autowired
    private UsuarioRepository usuarioRepository;


    public Usuario cadastraUsuario(Usuario pusuario)
    {
        return  usuarioRepository.save(pusuario);
    }

    public Usuario consultarUsuarioPorId(int id)
    {
        Optional<Usuario> objUsuario = usuarioRepository.findById(id);

        if (objUsuario.isPresent())
        {
            return objUsuario.get();
        }
        else
        {
            throw new UsuarioNaoEncontradoException();
        }
    }





}
